# Application C# - Centre Equestre ~ Mouaz MOHAMED

Centre Equestre est une application console développée en C# qui permet la gestion pour un haras. Il implémente une classe `Cheval` pour représenter les chevaux et fournit une démonstration de son utilisation via la classe `Program`.

---

## Structure de l'application
Cette applcication est organisé en deux fichiers principaux :

- `Cheval.cs`: Ce fichier contient la définition de la classe `Cheval`, qui représente les caractéristiques d'un cheval.
- `Program.cs`: Ce fichier contient la classe principale `Program`, qui démontre l'utilisation de la classe `Cheval`.

## Cas d'utilisation
```plantuml
left to right direction
:Utilisateur: as Utilisateur
package <uc>Centre_Equestre{
    Utilisateur --> (Ajouter un Cheval)
    Utilisateur --> (Modifier un Cheval)
    Utilisateur --> (Supprimer un Cheval)
    Utilisateur --> (Afficher les Détails d'un Cheval)
    Utilisateur --> (Afficher les Chevaux)
}
```

---

## Gestion des Chevaux

- La classe `Cheval` permet de représenter les caractéristiques d'un cheval, telles que son nom, son âge, sa taille, son poids et sa vitesse.
- Elle fournit des méthodes pour récupérer ces informations individuellement (`GetNom()`, `GetAge()`, `GetTaille()`, `GetPoids()`, `GetVitesse()`), ainsi qu'un constructeur pour initialiser ces valeurs.

---

## Exemple d'utilisation de l'application

- La classe `Program` démontre l'utilisation de la classe `Cheval` en créant une instance de `Cheval` avec des valeurs spécifiques (dans notre cas, le cheval s'appelle "Steve Jobs", a 5 ans, mesure 1.50m, pèse 650 kg et sa vitesse est initialisée à 0).
- Après avoir créé l'instance, elle affiche les détails du cheval à l'aide de la méthode `ToString()`.

---

## Classe Cheval
```csharp
using System;

namespace CentreEquestre
{
    class Cheval
    {
        // Déclaration des attributs privés de la classe Cheval
        private string nom;
        private int age;
        private decimal taille;
        private int poids;
        private int vitesse;

        // Constructeur par défaut de la classe Cheval
        public Cheval()
        {
            // Le constructeur par défaut est vide
        }

        // Constructeur paramétré de la classe Cheval
        public Cheval(string nom, int age, decimal taille, int poids, int vitesse)
        {
            // Initialisation des attributs avec les valeurs passées en paramètres
            this.nom = nom;
            this.age = age;
            this.taille = taille;
            this.poids = poids;
            // La vitesse est initialisée à 0 pour un nouveau cheval
            this.vitesse = 0;
        }

        // Méthodes pour récupérer les valeurs des attributs
        public string GetNom()
        {
            return this.nom;
        }
        public int GetAge()
        {
            return this.age;
        }
        public decimal GetTaille()
        {
            return this.taille;
        }
        public int GetPoids()
        {
            return this.poids;
        }
        public int GetVitesse()
        {
            return this.vitesse;
        }

        // Redéfinition de la méthode ToString pour afficher les détails du cheval
        public new string ToString()
        {
            // Formatage des informations du cheval sous forme d'une chaîne de caractères
            return string.Format("{0}, {1}, {2}, {3}, {4}", this.nom, this.age, this.taille, this.poids, this.vitesse);
        }
    }
}
```

---

### Classe Program
```csharp
using System;

namespace CentreEquestre
{
    class Program
    {
        static void Main(string[] args)
        {
            // Création d'une instance de Cheval avec des valeurs spécifiques
            Cheval c = new Cheval("Jolly Jumper", 3, 1.50m, 500, 0);
            
            // Affichage des détails du cheval à l'aide de la méthode ToString()
            Console.WriteLine(c.ToString());
        }
    }
}
```